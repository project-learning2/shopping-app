const express = require("express");

const router = express.Router();
const authControllers = require("../controllers/auth");
const adminControllers = require("../controllers/admin");

router.post("/login", authControllers.postLoginAdmin);
router.get("/get-all-products", adminControllers.getAllProducts);
router.get("/get-all-orders", adminControllers.getAllOrders);
router.post("/add-new-product", adminControllers.postAddNewProduct);
router.post("/edit-product/:productId", adminControllers.postEditProduct);
router.post("/delete-product/:productId", adminControllers.postDeleteProduct);

module.exports = router;
