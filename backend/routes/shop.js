const express = require("express");

const router = express.Router();

const shopControllers = require("../controllers/shop");
const authControllers = require("../controllers/auth");

router.post("/create-user", authControllers.postCreateUser);
router.post("/login", authControllers.postLogin);
router.get("/login/:sessionID", authControllers.getLogin);
router.post("/logout", authControllers.postLogout);

router.get("/get-top-trending", shopControllers.getTopTrending);
router.get("/get-product-by-id/:productId", shopControllers.getProductById);
router.get("/get-products", shopControllers.getAllProduct);
router.post("/add-to-cart", shopControllers.addToCart);
// router.get("/get-cart/:userId", shopControllers.getCart);
router.post("/create-order", shopControllers.postCreateOrder);
router.get("/get-history/:userId", shopControllers.getHistory);

module.exports = router;
// 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
