const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

exports.getTopTrending = (req, res, next) => {
  Product.find()
    .then((product) => {
      res.status(200).json({ toptrending: product.splice(0, 9) });
    })
    .catch((err) => {
      res.status(400).json({ message: "Some thing went wrong!!", err: err });
    });
};

exports.getProductById = (req, res, next) => {
  const productId = req.params.productId;
  Product.findOne({ _id: productId })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(400).json({ message: "Some thing went wrong!!", err: err });
    });
};

exports.getAllProduct = (req, res, next) => {
  Product.find()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(400).json({ message: "Some thing went wrong!!", err: err });
    });
};

exports.addToCart = (req, res, next) => {
  const userId = req.body.user;
  const item = req.body.item;
  User.findByIdAndUpdate(userId)
    .populate("cart.items.product")
    .exec()
    .then((user) => {
      if (user) {
        const cartItem = user.cart.items.find(
          (itemm) => itemm.product._id.toString() === item.product
        );
        if (cartItem) {
          cartItem.qty += item.qty;
          user.cart.totalPrice += cartItem.product.price * item.qty;
          user.save().then(() => {
            res.status(200).json({ message: "Item added to cart" });
          });
        } else {
          user.cart.items.push(item);
          Product.findById(item.product)
            .then((product) => {
              user.cart.totalPrice += product.price * item.qty;
              user.save().then(() => {
                res.status(200).json({ message: "Item added to cart" });
              });
            })
            .catch((err) => {
              res
                .status(400)
                .json({ message: "Some thing went wrong!!", err: err });
            });
        }
      } else {
        res.status(404).json({ message: "User not found" });
      }
    })
    .catch((err) => {
      res.status(400).json({ message: "Some thing went wrong!!", err: err });
    });
};

exports.postCreateOrder = (req, res, next) => {
  const order = req.body;
  const newOrder = new Order(order);
  newOrder
    .save()
    .then((result) => {
      Order.findById(result._id)
        .populate("products._id")
        .exec()
        .then((orderr) => {
          const productRender = orderr.products.map((el) => {
            return `<tr>
                      <td>${el._id.name}</td>
                      <td>
                        <img
                          src=${el._id.img1}
                          alt=""
                          width: "200px"
                        />
                      </td>
                      <td>${el._id.price
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND</td>
                      <td>${el.quantity}</td>
                      <td>${
                        el._id.price *
                        el.quantity
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                      } VND</td>
                    </tr>`;
          });
          const msg = {
            to: order.info.email,
            from: "locnguyenddbrvt@gmail.com",
            subject: "From Boutique Shop",
            text: "We'll contact with you to authenticate your order",
            html: `<html>
            <head>
              <style>
                * {
                  padding: 0;
                  margin: 0;
                }
                body {
                  background-color: black;
                  color: white;
                  height: fit-content;
                  width: 1024px;
                  line-height: 1.8;
                }
                .gird {
                  text-align: center;
                  display: grid;
                  grid-template-columns: 2fr auto 1fr auto 1fr;
                }
                .gird img {
                  width: 80px;
                }
                .border {
                  border: 1px solid white;
                }
              </style>
            </head>
            <body>
              <h3>Xin Chào ${order.info.fullName}</h3>
              <h5>PhoneNumber: ${order.info.phoneNumber}</h5>
              <h6>Address: ${order.info.address}</h6>
              <table>
              <thead>
                <tr>
                  <th>Tên Sản Phẩm</th>
                  <th>Hình ảnh</th>
                  <th>Giá</th>
                  <th>Số lượng</th>
                  <th>Thành tiền</th>
                </tr>
              </thead>
              <tbody>
                ${productRender}
              </tbody>
            </table>
              <h3>Tổng thanh toán: ${order.total
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND</h3>
              <h4>Cảm ơn bạn!!</h4>
            </body>
          </html>`,
          };
          sgMail
            .send(msg)
            .then(() => {
              console.log("Email sent");
            })
            .catch((error) => {
              console.error(error);
            });
        })
        .catch((err) => {
          console.log(err);
        });
      res.status(201).json({ message: "Create Order Success!!" });
    })
    .catch((err) => {
      res.status(400).json({ message: "Some thing went wrong!!", err: err });
    });
};

exports.getHistory = (req, res, next) => {
  const userId = req.params.userId;
  Order.find({ user: userId })
    .populate("products._id")
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(404).json({ message: "Some thing went wrong!!", err: err });
    });
};
