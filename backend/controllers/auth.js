const User = require("../models/User");
const bcrypt = require("bcryptjs");
const store = require("../models/Session").store;

exports.postCreateUser = (req, res, next) => {
  console.log(req.body);
  const fullName = req.body.fullName;
  const email = req.body.email;
  const password = req.body.password;
  const phoneNumber = req.body.phone;

  User.findOne({ email: email })
    .then((user) => {
      if (user) {
        return res
          .status(400)
          .json({ message: "Email has already been used!" });
      }
      return bcrypt.hash(password, 12).then((hashedPassword) => {
        const newUser = new User({
          password: hashedPassword,
          fullName: fullName,
          phoneNumber: phoneNumber,
          email: email,
        });
        newUser
          .save()
          .then(() => {
            console.log("save new User success!!");
            res.status(201).json({ message: "Create user success!!" });
          })
          .catch((err) => {
            console.log(err);
          });
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getLogin = (req, res, next) => {
  const sessionID = req.params.sessionID;
  store.get(sessionID, (err, session) => {
    if (err) {
      console.log("Error finding session", err);
    } else {
      //   console.log(session);
      if (session) {
        res.status(200).json({ user: session.user });
      } else {
        res.status(404).json({ message: "Not found sessions" });
      }
    }
  });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  req.session.isLoggerdIn = true;
  User.findOne({ email: email })
    .then((user) => {
      if (!user) {
        res.status(401).json({ message: "This user not exits!!" });
      }
      bcrypt
        .compare(password, user.password)
        .then((result) => {
          if (result) {
            req.session.user = user;
            return req.session.save((err) => {
              if (err) {
                console.log("err", err);
                return res.status(500).json({ message: "Server error" });
              }

              res.status(200).json({
                message: "Login Success!!",
                user: user,
                sessionId: req.sessionID,
              });
            });
          } else {
            res.status(401).json({ message: "Password's correct" });
          }
        })
        .catch((err) => {
          return res.status(401).json({ message: "Invalid email or password" });
        });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postLogout = (req, res, next) => {
  const sessionID = req.query.sessionID;
  store.destroy(sessionID, (err) => {
    if (err) {
      console.log("Some thing went wrong!!", err);
    } else {
      res.status(200).json({ message: "Logout Success!!" });
    }
  });
};

exports.postLoginAdmin = (req, res, next) => {
  const email = req.body.username;
  const password = req.body.password;
  User.findOne({ email: email })
    .then((user) => {
      if (!user) {
        res.status(401).json({ message: "This user not exits!!" });
      }
      bcrypt
        .compare(password, user.password)
        .then((result) => {
          if (result) {
            if (user.role === "client") {
              res.status(401).json({ message: "Not have access!" });
            } else {
              res.status(200).json({
                message: "Login Success!!",
                user: user,
              });
            }
          } else {
            res.status(401).json({ message: "Password's correct" });
          }
        })
        .catch((err) => {
          return res.status(401).json({ message: "Invalid email or password" });
        });
    })
    .catch((err) => {
      console.log(err);
    });
};
