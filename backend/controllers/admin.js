const Product = require("../models/Product");
const Order = require("../models/Order");

exports.getAllProducts = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.status(200).json(products);
    })
    .catch((err) => {
      res.status(500).json({ message: "Some thing went wrong!!", err: err });
    });
};

exports.getAllOrders = (req, res, next) => {
  Order.find()
    .populate("products._id")
    .exec()
    .then((orders) => {
      res.status(200).json(orders);
    })
    .catch((err) => {
      res.status(500).json(err);
    });
};

exports.postAddNewProduct = (req, res, next) => {
  const category = req.body.category;
  const name = req.body.name;
  const long_desc = req.body.longDesc;
  const short_desc = req.body.shortDesc;
  const price = req.body.price;
  const img1 = `https://shopping-app-node.herokuapp.com${req.files[0].path.slice(
    6
  )}`;
  const img2 = `https://shopping-app-node.herokuapp.com${req.files[1].path.slice(
    6
  )}`;
  const img3 = `https://shopping-app-node.herokuapp.com${req.files[2].path.slice(
    6
  )}`;
  const img4 = `https://shopping-app-node.herokuapp.com${req.files[3].path.slice(
    6
  )}`;

  const newProduct = new Product({
    category: category,
    img1: img1,
    img2: img2,
    img3: img3,
    img4: img4,
    price: price,
    short_desc: short_desc,
    long_desc: long_desc,
    name: name,
  });
  newProduct
    .save()
    .then(() => {
      res.status(201).json({ message: "Create new product success!!" });
    })
    .catch((err) => {
      res.status(500).json({ err: err, message: "Some thing went wrong!!" });
    });
};

exports.postEditProduct = (req, res, next) => {
  const productId = req.params.productId;
  const name = req.body.name;
  const category = req.body.category;
  const price = req.body.price;
  const short_desc = req.body.shortDesc;
  const long_desc = req.body.longDesc;

  const updateData = {
    name: name,
    category: category,
    price: price,
    short_desc: short_desc,
    long_desc: long_desc,
  };

  Product.findByIdAndUpdate(productId, updateData, {
    new: true,
    runValidators: true,
  })
    .then((product) => {
      res.status(200).json({ message: "Update data product success!1" });
    })
    .catch((err) => {
      res.status(500).json({ message: "Some thing went wrong !!", err: err });
    });
};

exports.postDeleteProduct = (req, res, next) => {
  const productId = req.params.productId;
  Product.findByIdAndRemove(productId)
    .then((productDelete) => {
      res.status(200).json({
        message: `Delete product with Id:${productDelete._id} success!!`,
      });
    })
    .catch((err) => {
      res.status(500).json({ message: "SOme thing went wrong!!", err: err });
    });
};
