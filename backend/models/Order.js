const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderSchema = new Schema({
  products: [
    {
      _id: { type: Schema.Types.ObjectId, ref: "Product" },
      quantity: { type: Number, required: true },
    },
  ],
  total: { type: Number, require: true },
  date: { type: Date, require: true },
  status: { type: String, enum: ["processing", "completed"], required: true },
  info: {
    fullName: { type: String, required: true },
    email: { type: String, required: true },
    phoneNumber: { type: Number, require: true },
    address: { type: String, require: true },
  },
  user: { type: Schema.Types.ObjectId, ref: "User" },
});

module.exports = mongoose.model("Order", orderSchema);
