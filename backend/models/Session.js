const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

exports.store = new MongoDBStore({
  uri: `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.jyxohlo.mongodb.net/${process.env.MONGO_DATABASE}`,
  collection: "sessions",
});
