const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  password: { type: String, require: true },
  fullName: { type: String, require: true },
  phoneNumber: { type: String, require: true },
  email: { type: String, require: true, unique: true },
  // cart: {
  //   items: [
  //     {
  //       product: {
  //         type: Schema.Types.ObjectId,
  //         ref: "Product",
  //       },
  //       qty: { type: Number },
  //     },
  //   ],
  //   totalPrice: { type: Number, default: 0 },
  // },
  isAdmin: { type: Boolean, default: false },
  role: {
    type: String,
    enum: ["counselors", "client", "admin"],
    default: "client",
  },
});

module.exports = mongoose.model("User", userSchema);
