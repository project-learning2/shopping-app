const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const session = require("express-session");
const multer = require("multer");
const fs = require("fs");
const helmet = require("helmet");
const compression = require("compression");
// const morgan = require("morgan");
const app = express();

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const store = require("./models/Session").store;
const user = {};

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/images");
  },
  filename: (req, file, cb) => {
    cb(null, "-" + file.originalname);
  },
});

app.use(helmet());
app.use(compression());
app.use(cors());
// app.use(morgan("combined"));

app.use(multer({ storage: fileStorage }).array("files", 5));
app.use(express.static(path.join(__dirname, "public")));
app.use("/images", express.static(path.join(__dirname, "images")));
app.use(bodyParser.json());
app.use(
  session({
    secret: "mysecret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);

app.use("/", shopRoutes);
app.use("/admin", adminRoutes);

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.jyxohlo.mongodb.net/${process.env.MONGO_DATABASE}`
  )
  .then(() => {
    const server = app.listen(process.env.PORT || 5000);
    console.log("Conected ShoppingApp's Database by MongoDB");
    const io = require("socket.io")(server, {
      cors: {
        origin: "*",
      },
    });
    io.on("connection", (socket) => {
      console.log("Client connected", socket.id);

      socket.on("disconnect", () => {
        console.log("Client disconnected", socket.id);
      });

      socket.on("username", (username) => {
        user[username] = socket;
      });

      socket.on("on-chat", (data) => {
        if (socket.id !== user["admin"].id) {
          user["admin"].emit("on-chat", data);
        } else {
          user[data.username].emit("on-chat", data.message);
        }
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });
