import { createBrowserRouter, RouterProvider } from "react-router-dom";

import Hotel from "./pages/Product/Product";
import LiveChat from "./pages/Live Chat/LiveChat";
import Login from "./pages/Login/Login";
import RootLayout from "./pages/RootLayout/RootLayout";
import DashBoard from "./pages/DashBoard/DashBoard";
import AddNewProduct from "./pages/AddNewProduct/AddNewProduct";
import EditProduct from "./pages/EditProduct/EditProduct";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/dashboard",
        element: <DashBoard />,
      },
      {
        path: "/products",
        element: <Hotel />,
      },
      {
        path: "/livechat",
        element: <LiveChat />,
      },
      {
        path: "/add-new-product",
        element: <AddNewProduct />,
      },
      {
        path: "/edit-product/:productId",
        element: <EditProduct />,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
