import { Outlet, useNavigate } from "react-router-dom";
import NavBar from "../../components/NavBar/NavBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { useSelector, useDispatch } from "react-redux";

import styles from "./RootLayout.module.css";
import { useEffect } from "react";
import { authActions } from "../../store/auth";

const RootLayout = () => {
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  const navigate = useNavigate();
  const userLogin = useSelector((state) => state.auth.userLogin);
  const dispatch = useDispatch();
  const titleClickHandle = () => {
    navigate("/");
  };
  useEffect(() => {
    if (!isAuth) {
      navigate("/login");
    } else {
      navigate("/products");
    }
  }, [isAuth]);
  return (
    <>
      <div className={styles["title-bar"]}>
        <div className={styles.title} onClick={titleClickHandle}>
          Admin Page
        </div>
        {isAuth && (
          <div className={styles.action}>
            <span>Hi, {userLogin.fullName} </span>
            <button
              onClick={() => {
                dispatch(authActions.logout());
              }}
            >
              <FontAwesomeIcon icon={faRightFromBracket} /> Logout
            </button>
          </div>
        )}
      </div>
      <div className={styles["root-layout"]}>
        {isAuth ? (
          <>
            <NavBar />
            <main>
              <Outlet />
            </main>
          </>
        ) : (
          <main>
            <Outlet />
          </main>
        )}
      </div>
    </>
  );
};

export default RootLayout;
