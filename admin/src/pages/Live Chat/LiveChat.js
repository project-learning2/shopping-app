import { faCircleUser } from "@fortawesome/free-regular-svg-icons";
import { faPaperPlane, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useRef, useState } from "react";
import openSocket from "socket.io-client";

import styles from "./LiveChat.module.css";

const LiveChat = () => {
  const [message, setMessage] = useState("");
  const [dataMess, setDataMess] = useState({});
  const [isActive, setIsActive] = useState(null);
  const socket = useRef();
  useEffect(() => {
    socket.current = openSocket(process.env.REACT_APP_API_URL + "/");

    socket.current.on("connect", () => {
      socket.current.emit("username", "admin");
    });

    socket.current.on("on-chat", (data) => {
      setDataMess((prevDataMess) => {
        if (!prevDataMess[data.username]) {
          return {
            ...prevDataMess,
            [data.username]: [`user: ${data.message}`],
          };
        } else {
          if (data.message === "/end") {
            const updatedDataMess = { ...prevDataMess };
            delete updatedDataMess[data.username];
            if (data.username === isActive) {
              setIsActive(null);
            }
            return updatedDataMess;
          } else {
            const updatedUserMessages = [
              ...prevDataMess[data.username],
              `user: ${data.message}`,
            ];
            return {
              ...prevDataMess,
              [data.username]: updatedUserMessages,
            };
          }
        }
      });
    });

    return () => {
      socket.current.disconnect();
    };
  }, []);

  // useEffect(() => {
  //   console.log(dataMess);
  // }, [dataMess]);

  const chooseChatHandle = (username) => {
    setIsActive(username);
  };

  const messRender =
    Object.keys(dataMess).length > 0 ? (
      Object.keys(dataMess).map((el) => {
        if (el !== "undefined") {
          return (
            <div
              key={el}
              className={
                isActive === el
                  ? `${styles.contact} ${styles.active}`
                  : styles.contact
              }
              onClick={() => chooseChatHandle(el)}
            >
              <div className={styles["content-contact"]}>
                <FontAwesomeIcon icon={faUser} />
                <h5>{el}</h5>
              </div>
            </div>
          );
        }
      })
    ) : (
      <div>0 contact</div>
    );
  const chatAreaRender =
    isActive &&
    dataMess[isActive].map((el, index) => {
      const messArr = el.split(": ");
      return messArr[0] === "user" ? (
        <div key={index} className={styles["mess-el"]}>
          <FontAwesomeIcon icon={faCircleUser} />
          <p className={styles["mess-content"]}>{messArr[1]}</p>
        </div>
      ) : (
        <div key={index} className={styles["mess-admin"]}>
          <p className={`${styles["mess-content-admin"]}`}>{messArr[1]}</p>
        </div>
      );
    });

  const changeMessageHandle = (events) => {
    setMessage(events.target.value);
  };
  const sendMessageHandle = (events) => {
    events.preventDefault();
    if (message !== "" && isActive !== null) {
      socket.current.emit("on-chat", {
        username: isActive,
        message: message,
      });
      setDataMess((prevDataMess) => {
        const updatedUserMessages = [
          ...prevDataMess[isActive],
          `admin: ${message}`,
        ];
        return {
          ...prevDataMess,
          [isActive]: updatedUserMessages,
        };
      });
      setMessage("");
    }
  };

  return (
    <div className={styles["live-chat"]}>
      <h3>Chat</h3>
      <h6>App / Chat</h6>
      <div className={styles["chat-box"]}>
        <div className={styles["left"]}>
          <div className={styles.search}>
            <input placeholder="Search Contact" />
          </div>
          <div className={styles["contact-list"]}>{messRender}</div>
        </div>
        <div className={styles["chat-display"]}>
          <div className={styles["chat-area"]}>
            <div>{chatAreaRender}</div>
          </div>
          <form className={styles["chat-input"]} onSubmit={sendMessageHandle}>
            <input
              type="text"
              placeholder="Input Message"
              onChange={changeMessageHandle}
              value={message}
            />
            <button type="submit" className={styles["send-btn"]}>
              <FontAwesomeIcon icon={faPaperPlane} />
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LiveChat;
