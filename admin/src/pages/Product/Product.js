import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import styles from "./Product.module.css";

const Hotel = () => {
  const navigate = useNavigate();
  const [hotelsData, setHotelsData] = useState(null);
  const [enteredSearch, setEnteredSearch] = useState("");
  const [dataRender, setDataRender] = useState(null);
  const userLogin = useSelector((state) => state.auth.userLogin);
  const fetchAllHotels = () => {
    axios
      .get(process.env.REACT_APP_API_URL + "/admin/get-all-products")
      .then((response) => {
        setHotelsData(response.data);
        // console.log(response.data);
      })
      .catch((err) => console.log(err));
  };
  useEffect(() => {
    if (userLogin.role === "counselors") {
      alert("You not have access!!");
      navigate("/livechat");
    }
    fetchAllHotels();
  }, []);

  const deleteHotelHandle = (productId) => {
    const confirm = window.confirm("You really want to delete??");
    if (confirm) {
      axios
        .post(
          `${process.env.REACT_APP_API_URL}/admin/delete-product/${productId}`
        )
        .then((response) => {
          alert(response.data.message);
          fetchAllHotels();
        })
        .catch((err) => {
          alert(err.response.data.message);
        });
    }
  };
  const addNewHandle = () => {
    navigate("/add-new-product");
  };
  const editHandle = (productId) => {
    navigate(`/edit-product/${[productId]}`);
  };
  const changeSearch = (events) => {
    setEnteredSearch(events.target.value);
  };
  useEffect(() => {
    if (enteredSearch === "") {
      setDataRender(hotelsData);
    } else {
      const dataSearch = hotelsData.filter((el) => {
        return el.name
          .trim()
          .toLowerCase()
          .includes(enteredSearch.trim().toLowerCase());
      });
      setDataRender(dataSearch);
    }
  }, [enteredSearch, hotelsData]);

  return (
    <div className={styles.hotels}>
      <h3>Products List</h3>
      <button className={styles["add-btn"]} onClick={addNewHandle}>
        Add New
      </button>
      <input
        className={styles.search}
        type="text"
        onChange={changeSearch}
        value={enteredSearch}
        placeholder="Enter Search"
      />
      <div className={styles.table}>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Img</th>
              <th>Category</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            {dataRender &&
              dataRender.map((el) => {
                return (
                  <tr key={el._id} className={styles.hotel}>
                    <td>{el._id}</td>
                    <td>{el.name}</td>
                    <td>
                      {el.price
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
                      VND
                    </td>
                    <td>
                      <img src={el.img1} alt="" />
                    </td>
                    <td>{el.category}</td>
                    <td>
                      <button
                        style={{
                          color: "#fff",
                          border: "1px dashed #179733",
                          backgroundColor: "#179733",
                          marginLeft: "5px",
                        }}
                        onClick={() => editHandle(el._id)}
                      >
                        Update
                      </button>
                      <button onClick={() => deleteHotelHandle(el._id)}>
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        {/* <div className={styles.page}>
          <div className={styles["page-display"]}>
            <p>1-8 of 8 </p>
            <div className={styles["page-actions"]}>
              <FontAwesomeIcon icon={faChevronLeft} />
              <FontAwesomeIcon icon={faChevronRight} />
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default Hotel;
