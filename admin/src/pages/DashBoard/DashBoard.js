import {
  faDollar,
  faFileCirclePlus,
  faUserPlus,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import styles from "./DashBoard.module.css";

const DashBoard = () => {
  const [data, setData] = useState(null);
  const navigate = useNavigate();
  const userLogin = useSelector((state) => state.auth.userLogin);
  useEffect(() => {
    if (userLogin.role === "counselors") {
      alert("You not have access!!");
      navigate("/livechat");
    }
    axios
      .get(process.env.REACT_APP_API_URL + "/admin/get-all-orders")
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className={styles.dashboard}>
      <h2>Dash Board</h2>
      <div className={styles["infor-board"]}>
        <div className={styles["board-el"]}>
          <h6>2</h6>
          <p>Clients</p>
          <div
            style={{
              color: "#aa8b00",
              backgroundColor: "#faedb1",
            }}
            className={styles["board-icon"]}
          >
            <FontAwesomeIcon icon={faUserPlus} />
          </div>
        </div>
        <div className={styles["board-el"]}>
          <h6>44.779.000VND</h6>
          <p>Earnings of Month</p>
          <div
            style={{
              color: "#119200",
              backgroundColor: "#bffdb7",
            }}
            className={styles["board-icon"]}
          >
            <FontAwesomeIcon icon={faDollar} />
          </div>
        </div>
        <div className={styles["board-el"]}>
          <h6>2</h6>
          <p>New Order</p>
          <div
            style={{
              color: "#8e009b",
              backgroundColor: "#f4c7ff",
            }}
            className={styles["board-icon"]}
          >
            <FontAwesomeIcon icon={faFileCirclePlus} />
          </div>
        </div>
      </div>
      <div className={styles.transactions}>
        <h3>Lastest Transactions</h3>
        <div className={styles.table}>
          <table>
            <thead>
              <tr>
                <th>ID User</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Total</th>
                <th>Delivery</th>
                <th>Status</th>
                <th>Detail</th>
              </tr>
            </thead>
            <tbody>
              {data &&
                data.map((el) => {
                  return (
                    <tr key={el._id}>
                      <td>{el.user}</td>
                      <td>{el.info.fullName}</td>
                      <td>{el.info.phoneNumber}</td>
                      <td>{el.info.address}</td>
                      <td>
                        {el.total
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
                        VND
                      </td>
                      <td>Chưa vận chuyển</td>
                      <td>Chưa thanh toán</td>
                      <td>
                        <button
                          style={{
                            padding: "8px 15px",
                            color: "#fff",
                            backgroundColor: "#1a8a36",
                            border: "none",
                            borderRadius: "4px",
                          }}
                        >
                          View
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default DashBoard;
