import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import styles from "./EditProduct.module.css";

const EditProduct = () => {
  const navigate = useNavigate();
  const params = useParams().productId;
  const [productName, setProductName] = useState("");
  const [category, setCategory] = useState("");
  const [shortDesc, setShortDesc] = useState("");
  const [longDesc, setLongDesc] = useState("");
  const [price, setPrice] = useState(0);
  const userLogin = useSelector((state) => state.auth.userLogin);
  //   const [productData, setProductData ] = useState(null);

  const changeProductNameHandle = (events) => {
    setProductName(events.target.value);
  };
  const changeCategoryHanle = (events) => {
    setCategory(events.target.value);
  };
  const changeShortDescHanle = (events) => {
    setShortDesc(events.target.value);
  };
  const changeLongDescHandle = (events) => {
    setLongDesc(events.target.value);
  };
  const changePriceHanle = (events) => {
    setPrice(events.target.value);
  };

  useEffect(() => {
    if (userLogin.role === "counselors") {
      alert("You not have access!!");
      navigate("/livechat");
    }
    axios
      .get(`${process.env.REACT_APP_API_URL}/get-product-by-id/${params}`)
      .then((response) => {
        const productData = response.data;
        setProductName(productData.name);
        setCategory(productData.category);
        setPrice(productData.price);
        setShortDesc(productData.short_desc);
        setLongDesc(productData.long_desc);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const submitHandle = (events) => {
    events.preventDefault();
    if (
      productName.trim().length === 0 ||
      category.trim().length === 0 ||
      price === 0 ||
      shortDesc.trim().length === 0 ||
      longDesc.trim().length === 0
    ) {
      alert("Need field full input!!!");
    } else {
      const dataForm = new FormData();
      dataForm.append("name", productName);
      dataForm.append("category", category);
      dataForm.append("shortDesc", shortDesc);
      dataForm.append("price", price);
      dataForm.append("longDesc", longDesc);

      axios
        .post(
          `${process.env.REACT_APP_API_URL}/admin/edit-product/${params}`,
          dataForm,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((response) => {
          alert(response.data.message);
          navigate("/products");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  return (
    <form className={styles.form} onSubmit={submitHandle}>
      <div className={styles.input}>
        <label>Product Name</label>
        <input
          placeholder="Enter Product Name"
          value={productName}
          onChange={changeProductNameHandle}
          type="text"
        />
      </div>
      <div className={styles.input}>
        <label>Category</label>
        <select onChange={changeCategoryHanle} value={category}>
          <option value={""}>Choose Category</option>
          <option value="iphone">Iphone</option>
          <option value="ipad">Ipad</option>
          <option value="airpod">Airpod</option>
          <option value="watch">Watch</option>
          <option value="mac">Mac</option>
        </select>
      </div>
      <div className={styles.input}>
        <label>Price</label>
        <input
          placeholder="Enter Price"
          onChange={changePriceHanle}
          value={price}
          type="number"
        />
      </div>
      <div className={styles.input}>
        <label>Short Description</label>
        <input
          placeholder="Enter Short Description"
          onChange={changeShortDescHanle}
          value={shortDesc}
          type="text"
        />
      </div>
      <div className={styles.input}>
        <label>Long Description</label>
        <input
          placeholder="Enter Long Description"
          onChange={changeLongDescHandle}
          value={longDesc}
          type="text"
        />
      </div>
      <div className={styles.input}>
        <button type="submit">Submit</button>
      </div>
    </form>
  );
};

export default EditProduct;
