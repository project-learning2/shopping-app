import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faWhiskeyGlass,
  faRightLeft,
  faHeadset,
  faDashboard,
} from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-regular-svg-icons";
// import { authActions } from "../../store/auth";
// import { useDispatch } from "react-redux";

import styles from "./NavBar.module.css";
import { faDocker } from "@fortawesome/free-brands-svg-icons";
const NavBar = () => {
  // const dispatch = useDispatch();
  return (
    <header className={styles["nav-bar"]}>
      <nav className={styles.nav}>
        <ul>
          <li>
            <h4>Admin</h4>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faDashboard} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/dashboard"
              >
                Dashboard
              </NavLink>
            </div>
            {/* <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faUser} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/test"
              >
                Users
              </NavLink>
            </div> */}
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faDocker} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/products"
              >
                Products
              </NavLink>
            </div>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faWhiskeyGlass} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/room"
              >
                Orders
              </NavLink>
            </div>
            {/* <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faRightLeft} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/transaction"
              >
                Transactions
              </NavLink>
            </div> */}
          </li>
          <li>
            <h4>COUNSELORS</h4>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faHeadset} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/livechat"
              >
                Live Chat
              </NavLink>
            </div>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default NavBar;
