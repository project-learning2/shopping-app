import { useSelector, useDispatch } from "react-redux";
import React, { useCallback, useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrashCan,
  faGift,
  faArrowRight,
  faArrowLeft,
  faCaretLeft,
  faCaretRight,
} from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

import { cartActions } from "../store/cart";
import styles from "./ShoppingCart.module.css";
import Button from "./UI/Button";
import axios from "axios";

const ShoppingCart = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const itemCart = useSelector((state) => state.cart.cart);
  const [dataProduct, setDataProduct] = useState(null);
  // const userLogin = useSelector((state) => state.auth.userLogin);

  useEffect(() => {
    window.localStorage.setItem("itemCart", JSON.stringify(itemCart));
  }, [itemCart]);

  useEffect(() => {
    // console.log(itemCart);
    // window.localStorage.removeItem("itemCart");
    axios
      .get(`${process.env.REACT_APP_API_URL}/get-products`)
      .then((response) => {
        setDataProduct(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const decrementHandler = (value) => {
    if (value.quantity > 1) {
      dispatch(cartActions.decrement(value));
    } else {
      dispatch(cartActions.delete_cart(value));
    }
  };
  const incrementHandler = (value) => {
    dispatch(cartActions.increment(value));
  };
  const clickNavCheckBoxHandler = () => {
    navigate("/checkout");
  };
  const clickNavShopHandler = () => {
    navigate("/shop");
  };

  let total = 0;
  const dataRender = dataProduct ? (
    itemCart.map((el) => {
      const dataItem = dataProduct.find((item) => item._id === el.id);
      total += el.quantity * dataItem.price;
      return (
        <tr key={el.id} className={styles["el-cart"]}>
          <td>
            <img src={dataItem.img1} alt="" />
          </td>
          <td className={styles.name}>
            <h4>{dataItem.name}</h4>
          </td>
          <td>
            <div className={styles.price}>
              {dataItem.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
              VND
            </div>
          </td>
          <td>
            <div className={styles.quantity}>
              <span
                onClick={() => decrementHandler(el)}
                className={styles["add-btn"]}
              >
                <FontAwesomeIcon icon={faCaretLeft} />
              </span>
              <span> {el.quantity} </span>
              <span
                onClick={() => incrementHandler(el)}
                className={styles["subtract-btn"]}
              >
                <FontAwesomeIcon icon={faCaretRight} />
              </span>
            </div>
          </td>
          <td>
            <div className={styles.price}>
              {(el.quantity * dataItem.price)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
              VND
            </div>
          </td>
          <td>
            <div className={styles.remove}>
              <FontAwesomeIcon icon={faTrashCan} />
            </div>
          </td>
        </tr>
      );
    })
  ) : (
    <tr>
      <td>
        <div>Loadinggg..........</div>
      </td>
    </tr>
  );
  return (
    <div className={styles["shopping-cart"]}>
      <h2>SHOPPING CART</h2>
      <div className={styles.content}>
        <div className={styles["cart-infor"]}>
          <table className={styles.table}>
            <thead>
              <tr>
                <th>IMAGE</th>
                <th>PRODUCT</th>
                <th>PRICE</th>
                <th>QUANTITY</th>
                <th>TOTAL</th>
                <th>REMOVE</th>
              </tr>
            </thead>
            <tbody>{dataRender}</tbody>
          </table>
        </div>
        <div className={styles["cart-total"]}>
          <h2>CART TOTAL</h2>
          <div className={styles["subtotal"]}>
            <h4>SUBTOTAL</h4>
            <h5>
              {total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND
            </h5>
          </div>
          <div className={styles["total"]}>
            <h4>TOTAL</h4>
            <h5>
              {total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND
            </h5>
          </div>{" "}
          <div className={styles["redeem-form"]}>
            <input id="redeem" placeholder="Enter your coupon" />
            <Button>
              <FontAwesomeIcon icon={faGift} />
              <span> Apply coupon</span>
            </Button>
          </div>
        </div>
        <div className={styles["nav-actions"]}>
          <div onClick={clickNavShopHandler} className={styles["shopping"]}>
            <p>
              <FontAwesomeIcon icon={faArrowLeft} />
              <span> Continue shopping</span>
            </p>
          </div>
          <div onClick={clickNavCheckBoxHandler} className={styles.check}>
            <p>
              <span>Proceed to checkbox </span>
              <FontAwesomeIcon icon={faArrowRight} />
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShoppingCart;
