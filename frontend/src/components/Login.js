import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useCookies } from "react-cookie";

import styles from "./Login.module.css";
import Button from "./UI/Button";
import { authActions } from "../store/auth";
import axios from "axios";

const Login = (props) => {
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [message, setMessage] = useState(null);
  const [cookies, setCookie] = useCookies(["sessionID"]);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const changeEmailHandler = (events) => {
    setEnteredEmail(events.target.value);
  };
  const changePasswordHandler = (events) => {
    setEnteredPassword(events.target.value);
  };
  const submitFormHandler = (events) => {
    events.preventDefault();
    if (
      enteredEmail.trim().length === 0 ||
      enteredPassword.trim().length <= 8
    ) {
      setMessage(<p>Value input not valid</p>);
    } else {
      axios
        .post(process.env.REACT_APP_API_URL + "/login", {
          email: enteredEmail,
          password: enteredPassword,
        })
        .then((response) => {
          setCookie("sessionID", response.data.sessionId, { path: "/" });
          alert(response.data.message);
          dispatch(authActions.login(response.data.user));
          navigate("/");
        })
        .catch((err) => {
          setMessage(err.response.data.message);
        });
    }
    return;
  };

  return (
    <form onSubmit={submitFormHandler} className={styles["sign-up"]}>
      <h3>Login</h3>
      <div className={styles["message-alert"]}>{message}</div>
      <div className={styles["input-container"]}>
        <input
          placeholder="Email"
          type="email"
          id="e-mail"
          onChange={changeEmailHandler}
          value={enteredEmail}
        />
        <input
          placeholder="Password"
          type="password"
          onChange={changePasswordHandler}
          value={enteredPassword}
        />
      </div>
      <div className={styles.actions}>
        <Button type="submit">SIGN IN</Button>
        <p>
          Create an account? <Link to="/login/signup">Sign up</Link>
        </p>
      </div>
    </form>
  );
};

export default Login;
