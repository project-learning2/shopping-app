import axios from "axios";
import { useEffect, useState } from "react";
import styles from "./ProductList.module.css";
import ProductPopup from "./ProductPopup";

const ProductList = () => {
  const [isHovered, setIsHovered] = useState(null);
  const [dataProduct, setDataProduct] = useState(null);
  const [isActivedId, setIsActivedId] = useState(null);

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_URL + "/get-top-trending")
      .then((response) => {
        setDataProduct(response.data.toptrending);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  // XỬ lý modal Popup
  const clickProductHandler = (idProduct) => {
    setIsActivedId(idProduct);
  };
  const closePopup = () => {
    setIsActivedId(null);
  };
  // Xử lý hovered
  const mouseEnteredHandler = (idProduct) => {
    setIsHovered(idProduct);
  };
  const mouseLeaveHandler = () => {
    setIsHovered(null);
  };

  const dataRender = dataProduct ? (
    <>
      {dataProduct.map((el) => (
        <div key={el._id}>
          <div
            className={`${styles.product} ${
              isHovered === el._id ? styles["isHovered"] : undefined
            }`}
            onClick={() => clickProductHandler(el._id)}
            onMouseEnter={() => mouseEnteredHandler(el._id)}
            onMouseLeave={mouseLeaveHandler}
          >
            <div className={styles["product-img"]}>
              <img src={el.img1} alt={el._id.$oid} />
            </div>
            <h3>{el.name}</h3>
            <p>{el.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</p>
          </div>
          {isActivedId === el._id && (
            <ProductPopup key={el._id} data={el} onClosePopup={closePopup} />
          )}
        </div>
      ))}
    </>
  ) : (
    <p>Loadinggg.....</p>
  );

  return (
    <div className={styles["product-list"]}>
      <div className={styles.title}>
        <h4>MADE THE HARD WAY</h4>
        <h2>TOP TRENDING PRODUCTS</h2>
      </div>
      <div className={styles["products-container"]}>{dataRender}</div>
    </div>
  );
};

export default ProductList;
