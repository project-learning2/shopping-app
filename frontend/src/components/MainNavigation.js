import { Link, NavLink, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping, faUser } from "@fortawesome/free-solid-svg-icons";
import { useSelector, useDispatch } from "react-redux";
import { useCookies } from "react-cookie";

import { authActions } from "../store/auth";
import styles from "./MainNavigation.module.css";
import axios from "axios";

const MainNavigation = () => {
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  const userLogin = useSelector((state) => state.auth.userLogin);
  const navigate = useNavigate();
  const [cookies, setCookie] = useCookies(["sessionID"]);
  const sessionIDCookie = cookies["sessionID"];
  const logoutHandler = () => {
    axios
      .post(
        `${process.env.REACT_APP_API_URL}/logout?sessionID=${sessionIDCookie}`,
        "Hi"
      )
      .then((response) => {
        // console.log(response);
        dispatch(authActions.logout());
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <header className={styles.header}>
      <nav className={styles.nav}>
        <ul>
          <li className={styles["left-nav"]}>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/"
            >
              Home
            </NavLink>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/shop"
            >
              Shop
            </NavLink>
          </li>
          <li className={styles.title}>BOUTIQUE</li>
          <li className={styles["right-nav"]}>
            {isAuth ? (
              <>
                {" "}
                <NavLink
                  className={({ isActive }) =>
                    isActive ? styles.active : undefined
                  }
                  to="/cart"
                >
                  <FontAwesomeIcon icon={faCartShopping} />
                  Cart
                </NavLink>
                <span
                  // className={({ isActive }) =>
                  //   isActive ? styles.active : undefined
                  // }
                  onClick={() => {
                    navigate(`/history/${userLogin._id}`);
                  }}
                >
                  <FontAwesomeIcon icon={faUser} />
                  {userLogin.fullName}
                </span>
                <Link onClick={logoutHandler}>Logout</Link>
              </>
            ) : (
              <>
                {" "}
                <NavLink
                  className={({ isActive }) =>
                    isActive ? styles.active : undefined
                  }
                  to="/cart"
                >
                  <FontAwesomeIcon icon={faCartShopping} />
                  Cart
                </NavLink>
                <NavLink
                  className={({ isActive }) =>
                    isActive ? styles.active : undefined
                  }
                  to="/login/login"
                >
                  <FontAwesomeIcon icon={faUser} />
                  Login
                </NavLink>
              </>
            )}
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;
