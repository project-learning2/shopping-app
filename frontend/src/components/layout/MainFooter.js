import styles from "./Footer.module.css";

const MainFooter = () => {
  return (
    <footer className={styles.footer}>
      <ul className={styles.content}>
        <li className={styles["footer-column"]}>
          <h3>CUSTOMER SERVICES</h3>
          <ul>
            <li>Help & Contact Us</li>
            <li>Return & Refund</li>
            <li>Online Stores</li>
            <li>Terms & Conditions</li>
          </ul>
        </li>
        <li className={styles["footer-column"]}>
          <h3>COMPANY</h3>
          <ul>
            <li>What We Do</li>
            <li>Available Servies</li>
            <li>Lastest Posts</li>
            <li>FAQs</li>
          </ul>
        </li>
        <li className={styles["footer-column"]}>
          <h3>SOCIAL MEDIA</h3>
          <ul>
            <li>Twitter</li>
            <li>Intagram</li>
            <li>FaceBook</li>
            <li>Printerest</li>
          </ul>
        </li>
      </ul>
    </footer>
  );
};

export default MainFooter;
