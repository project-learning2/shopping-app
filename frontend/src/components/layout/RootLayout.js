import { Outlet } from "react-router-dom";
import { useEffect } from "react";
import { useCookies } from "react-cookie";
import { useDispatch } from "react-redux";
import axios from "axios";

import styles from "./Layout.module.css";
import MainNavigation from "../MainNavigation";
import MainFooter from "./MainFooter";
import LiveChat from "../LiveChat";
import { authActions } from "../../store/auth";

const RootLayout = () => {
  const [cookies, setCookie] = useCookies(["sessionID"]);
  const sessionIDCookie = cookies["sessionID"];
  const dispatch = useDispatch();

  useEffect(() => {
    if (sessionIDCookie) {
      axios
        .get(`https://shopping-app-node.herokuapp.com/login/${sessionIDCookie}`)
        .then((response) => {
          dispatch(authActions.login(response.data.user));
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [sessionIDCookie]);
  return (
    <>
      <LiveChat />
      <MainNavigation />
      <main className={styles.main}>
        <Outlet />
      </main>
      <MainFooter />
    </>
  );
};

export default RootLayout;
