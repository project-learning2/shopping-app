import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookMessenger } from "@fortawesome/free-brands-svg-icons";
import ReactDOM from "react-dom";
import {
  faUserTie,
  faPaperclip,
  faPaperPlane,
  faFaceSmile,
  faCircleUser,
} from "@fortawesome/free-solid-svg-icons";
import openSocket from "socket.io-client";
import { useSelector } from "react-redux";

import styles from "./LiveChat.module.css";
import { useState, useEffect, useRef } from "react";

const LiveChat = () => {
  const [showLiveChat, setShowLiveChat] = useState(false);
  const liveChatRoot = document.getElementById("live-chat");
  const [clientMess, setClientMess] = useState([]);
  const [enteredMess, setEnteredMess] = useState("");
  const userLogin = useSelector((state) => state.auth.userLogin);

  const socket = useRef();
  useEffect(() => {
    if (userLogin) {
      socket.current = openSocket(process.env.REACT_APP_API_URL + "/");

      socket.current.on("connect", () => {
        socket.current.emit("username", userLogin.email);
      });

      socket.current.on("on-chat", (message) => {
        setClientMess((preClientMess) => {
          return [...preClientMess, `admin: ${message}`];
        });
      });

      return () => {
        socket.current.disconnect();
      };
    }
  }, [userLogin]);

  const showLiveChatHandler = () => {
    if (showLiveChat) {
      setShowLiveChat(false);
    } else {
      setShowLiveChat(true);
    }
  };

  const changeMessHanle = (events) => {
    setEnteredMess(events.target.value);
  };

  const sendMessHandle = (events) => {
    events.preventDefault();
    if (enteredMess !== "") {
      if (userLogin) {
        socket.current.emit("on-chat", {
          username: userLogin.email,
          message: enteredMess,
        });
        setEnteredMess("");
        setClientMess((preClientMess) => {
          return [...preClientMess, `user: ${enteredMess}`];
        });
      }
    }
  };

  const messRender =
    clientMess.length > 0 &&
    clientMess.map((el) => {
      const arrMess = el.split(": ");
      return arrMess[0] === "admin" ? (
        <div className={styles["admin-chat"]}>
          <FontAwesomeIcon icon={faCircleUser} />
          <div className={styles["admin-chat-content"]}>
            <p>{arrMess[1]}</p>
          </div>
        </div>
      ) : (
        <div className={styles["user-chat"]}>
          <div className={styles["user-content-chat"]}>
            <p>{arrMess[1]}</p>
          </div>
        </div>
      );
    });

  return ReactDOM.createPortal(
    <>
      <div onClick={showLiveChatHandler} className={styles.liveChat}>
        <FontAwesomeIcon icon={faFacebookMessenger} />
      </div>
      <div
        className={`${styles["chat-box"]} ${
          showLiveChat ? undefined : styles.hide
        }`}
      >
        {userLogin ? (
          <>
            <div className={styles.header}>
              <h3>Customer Support</h3>
              <button>Let's Chat App</button>
            </div>
            <div className={styles.content}>{messRender}</div>
            <form className={styles.footer} onSubmit={sendMessHandle}>
              <div className={styles.avatar}>
                <FontAwesomeIcon icon={faUserTie} />
              </div>
              <input
                type="text"
                id="message"
                placeholder="Enter Message"
                value={enteredMess}
                onChange={changeMessHanle}
              />
              <div className={styles.actions}>
                <div className={styles["send-file"]}>
                  <FontAwesomeIcon icon={faPaperclip} />
                </div>
                <div className={styles["send-icon"]}>
                  <FontAwesomeIcon icon={faFaceSmile} />
                </div>
                <button type="submit" className={styles["send-message"]}>
                  <FontAwesomeIcon icon={faPaperPlane} />
                </button>
              </div>
            </form>
          </>
        ) : (
          <div>Pls Login</div>
        )}
      </div>
    </>,
    liveChatRoot
  );
};

export default LiveChat;
