import { useNavigate } from "react-router-dom";

import styles from "./Banner.module.css";
import BannerImg from "../asset/image/banner1.jpg";
import Button from "./UI/Button";

const Banner = () => {
  const navigate = useNavigate();
  const clickBtnHandler = () => {
    navigate("/shop");
  };
  return (
    <div className={styles.banner}>
      <img src={BannerImg} alt="bannerimg" />
      <div className={styles.descriptions}>
        <p>NEW INSPIRATION 2020</p>
        <h2>20% OFF ON NEW SEASON</h2>
        <Button className={styles["banner-btn"]} onClick={clickBtnHandler}>
          Browse collections
        </Button>
      </div>
    </div>
  );
};

export default Banner;
