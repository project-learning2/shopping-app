import { useState } from "react";
import { useNavigate } from "react-router-dom";

import styles from "./Category.module.css";
import iphoneImg from "../asset/image/product_1.png";
import imacImg from "../asset/image/product_2.png";
import ipadImg from "../asset/image/product_3.png";
import watchImg from "../asset/image/product_4.png";
import airpodImg from "../asset/image/product_5.png";

const Catagory = () => {
  const [isHovered, setIsHovered] = useState(null);
  const navigate = useNavigate();

  const mouseEnteredHandler = (index) => {
    setIsHovered(index);
  };
  const mouseLeaveHandler = () => {
    setIsHovered(null);
    // console.log(isHovered);
  };

  const clickIphoneHandler = () => {
    navigate("/shop");
  };
  const clickImacHandler = () => {
    navigate("/shop");
  };
  const clickIpadHandler = () => {
    navigate("/shop");
  };
  const clickWatchHandler = () => {
    navigate("/shop");
  };
  const clickAirpodHandler = () => {
    navigate("/shop");
  };
  return (
    <div className={styles.category}>
      <div className={styles.tittle}>
        <p>CAREFULLY CREATED COLLECTIONS</p>
        <h2>BROWSE OUR CATEGORIES</h2>
      </div>
      <div className={styles["description-img"]}>
        <div className={styles["product-column"]}>
          <div
            key="iphone"
            className={
              isHovered === 0
                ? `${styles.product} ${styles.isHovered}`
                : styles.product
            }
            onMouseEnter={() => {
              mouseEnteredHandler(0);
            }}
            onMouseLeave={mouseLeaveHandler}
            onClick={clickIphoneHandler}
          >
            <img src={iphoneImg} alt="category-iphone" />
          </div>
          <div
            key="mac"
            className={
              isHovered === 1
                ? `${styles.product} ${styles.isHovered}`
                : styles.product
            }
            onMouseEnter={() => {
              mouseEnteredHandler(1);
            }}
            onMouseLeave={mouseLeaveHandler}
            onClick={clickImacHandler}
          >
            <img src={imacImg} alt="category-mac" />
          </div>
        </div>
        <div className={styles["product-column"]}>
          <div
            key="ipad"
            className={
              isHovered === 2
                ? `${styles.product} ${styles.isHovered}`
                : styles.product
            }
            onMouseEnter={() => {
              mouseEnteredHandler(2);
            }}
            onMouseLeave={mouseLeaveHandler}
            onClick={clickIpadHandler}
          >
            <img src={ipadImg} alt="ipad" />
          </div>
          <div
            key="wacth"
            className={
              isHovered === 3
                ? `${styles.product} ${styles.isHovered}`
                : styles.product
            }
            onMouseEnter={() => {
              mouseEnteredHandler(3);
            }}
            onMouseLeave={mouseLeaveHandler}
            onClick={clickWatchHandler}
          >
            <img src={watchImg} alt="watch" />
          </div>
          <div
            key="airpod"
            className={
              isHovered === 4
                ? `${styles.product} ${styles.isHovered}`
                : styles.product
            }
            onMouseEnter={() => {
              mouseEnteredHandler(4);
            }}
            onMouseLeave={mouseLeaveHandler}
            onClick={clickAirpodHandler}
          >
            <img src={airpodImg} alt="air-pod" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Catagory;
