import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import axios from "axios";

import styles from "./BillDetail.module.css";
import { useNavigate } from "react-router-dom";

const BillDetail = () => {
  const itemCart = useSelector((state) => state.cart.cart);
  const userLogin = useSelector((state) => state.auth.userLogin);
  const navigate = useNavigate();
  const [dataProduct, setDataProduct] = useState(null);
  const [enteredFullName, setEnteredFullName] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPhone, setEnteredPhone] = useState("");
  const [enteredAddress, setEnteredAddress] = useState("");
  let total = 0;

  const changeFullNameHandler = (events) => {
    setEnteredFullName(events.target.value);
  };
  const changeEmailHandler = (events) => {
    setEnteredEmail(events.target.value);
  };
  const changePhoneHandler = (events) => {
    setEnteredPhone(events.target.value);
  };
  const changeAddressHandler = (events) => {
    setEnteredAddress(events.target.value);
  };

  const submitHandle = (events) => {
    events.preventDefault();
    if (!userLogin) {
      alert("Login to order now!!");
    } else {
      if (
        enteredFullName.trim().length === 0 ||
        enteredAddress.trim().length === 0 ||
        enteredPhone.trim().length === 0 ||
        enteredAddress.trim().length === 0
      ) {
        alert("enter Form!!");
      } else {
        const products = itemCart.map((el) => {
          return {
            _id: el.id,
            quantity: el.quantity,
          };
        });
        const dataOrder = {
          products: products,
          total: total,
          date: new Date(),
          status: "processing",
          info: {
            fullName: enteredFullName,
            email: enteredEmail,
            phoneNumber: enteredPhone,
            address: enteredAddress,
          },
          user: userLogin._id,
        };
        console.log(dataOrder);
        axios
          .post(`${process.env.REACT_APP_API_URL}/create-order`, dataOrder)
          .then((response) => {
            alert(response.data.message);
            navigate(`/history/${userLogin._id}`);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  };

  useEffect(() => {
    if (userLogin) {
      setEnteredFullName(userLogin.fullName);
      setEnteredEmail(userLogin.email);
      setEnteredPhone(userLogin.phoneNumber);
    }
    axios
      .get(`${process.env.REACT_APP_API_URL}/get-products`)
      .then((response) => {
        setDataProduct(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const dataRender = dataProduct ? (
    itemCart.map((el) => {
      const dataItem = dataProduct.find((item) => item._id === el.id);
      total += el.quantity * dataItem.price;
      return (
        <div key={el.id} className={styles["el-order"]}>
          <h5>{dataItem.name}</h5>
          <p>
            {dataItem.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
            VNDx{el.quantity}
          </p>
        </div>
      );
    })
  ) : (
    <p>Loadinggg..........</p>
  );

  return (
    <div className={styles["bill-detail"]}>
      <h2>BILL DETAILS</h2>
      <div className={styles.content}>
        <form className={styles["form-order"]} onSubmit={submitHandle}>
          <div className={styles["full-name"]}>
            <label>FULL NAME:</label>
            <input
              type="text"
              id="fullName"
              placeholder="Enter Your Full Name Here"
              value={enteredFullName}
              onChange={changeFullNameHandler}
            />
          </div>
          <div className={styles["e-mail"]}>
            <label>EMAIL:</label>
            <input
              type="email"
              id="fullName"
              placeholder="Enter Your Email Here"
              value={enteredEmail}
              onChange={changeEmailHandler}
            />
          </div>
          <div className={styles["phone"]}>
            <label>PHONE NUMBER:</label>
            <input
              type="tel"
              id="fullName"
              placeholder="Enter Your Phone Number Here"
              value={enteredPhone}
              onChange={changePhoneHandler}
            />
          </div>
          <div className={styles.address}>
            <label>ADDRESS:</label>
            <input
              type="text"
              id="Address"
              placeholder="Enter Your Address Here"
              value={enteredAddress}
              onChange={changeAddressHandler}
            />
          </div>
          <button type="submit">Place Order</button>
        </form>
        <div className={styles["desc-box"]}>
          <h3>YOUR ORDER</h3>
          <div className={styles["desc-detail"]}>
            {dataRender}
            <div className={styles.total}>
              <h4>TOTAL</h4>
              <p>
                {total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BillDetail;
