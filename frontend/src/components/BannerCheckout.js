import styles from "./BannerCheckOut.module.css";

const BannerCheckout = () => {
  const style = {
    color: "#6666",
  };
  return (
    <div className={styles["banner-checkout"]}>
      <h2>CART</h2>
      <div>
        <span>HOME / </span>
        <span>CART / </span>
        <span style={style}>CHECKOUT</span>
      </div>
    </div>
  );
};

export default BannerCheckout;
