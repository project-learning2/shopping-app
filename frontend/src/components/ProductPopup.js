import React from "react";
import ReactDOM from "react-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

import styles from "./ProductPopup.module.css";
import Button from "./UI/Button";

const ProductPopup = (props) => {
  const popupRoot = document.getElementById("root-popup");
  const navigate = useNavigate();
  const viewDetilHandle = (productId) => {
    navigate(`/detail/${productId}`);
  };

  return ReactDOM.createPortal(
    <>
      <div className={styles["back-drop"]}>
        <div key={props.data._id} className={`${styles["product-popup"]}`}>
          <div className={styles["img-popup"]}>
            <img src={props.data.img1} alt={props.data._id.$oid} />
          </div>
          <div className={styles.descriptions}>
            <div>
              {" "}
              <h2>{props.data.name}</h2>
              <h4>
                {props.data.price
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
              </h4>
              <p>{props.data.short_desc}</p>
              <button
                className={styles["view-detail-btn"]}
                onClick={() => viewDetilHandle(props.data._id)}
              >
                <FontAwesomeIcon icon={faCartShopping} />
                View Detail
              </button>
              <div className={styles["close-btn"]}>
                <Button onClick={props.onClosePopup}>X</Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>,
    popupRoot
  );
};

export default ProductPopup;
