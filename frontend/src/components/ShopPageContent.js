import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
// import { CSSTransition } from "react-transition-group";

import styles from "./ShopPageContent.module.css";

const ShopPageContent = () => {
  const navigate = useNavigate();
  const [dataProduct, setDataProduct] = useState(null);
  const [productName, setProductName] = useState("all");

  const fetchAPI = useCallback(async () => {
    const response = await fetch(
      process.env.REACT_APP_API_URL + "/get-products"
    );
    if (!response.ok) {
      throw new Error("Some thing went wrong!");
    }
    const data = await response.json();
    setDataProduct(data);
  }, []);
  useEffect(() => {
    fetchAPI();
  }, [fetchAPI]);
  const clickProductElHandler = (idProduct) => {
    navigate(`/detail/${idProduct}`);
  };

  const dataRender = dataProduct ? (
    dataProduct.map((el) => (
      <div
        key={el._id}
        className={`${styles["product-el"]} ${
          productName === "all"
            ? undefined
            : !(productName === el.category)
            ? styles.hiden
            : undefined
        }`}
        onClick={() => clickProductElHandler(el._id)}
      >
        <div className={styles["product-img"]}>
          <img src={el.img1} alt="" />
        </div>
        <div className={styles.text}>
          <h4>{el.name}</h4>
          <p>{el.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</p>
        </div>
      </div>
    ))
  ) : (
    <p>Loadinggg............</p>
  );

  const filterProductHandler = (productName) => {
    setProductName(productName);
  };

  return (
    <div className={styles["shop-page-content"]}>
      <nav className={styles.nav}>
        <h2>CATEGORIES</h2>
        <div className={styles["nav-options-container"]}>
          <div className={styles["title-nav-spec"]}>APPLE</div>
          <div
            onClick={() => filterProductHandler("all")}
            className={`${productName === "all" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
          >
            All
          </div>
          <div className={styles["title-nav"]}>IPHONE & IMAC</div>
          <div
            className={`${productName === "iphone" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("iphone")}
          >
            Iphone
          </div>
          <div
            className={`${productName === "ipad" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("ipad")}
          >
            Ipad
          </div>
          <div
            className={`${productName === "macbook" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("macbook")}
          >
            MacBook
          </div>
          <div className={styles["title-nav"]}>WIRELESS</div>
          <div
            className={`${productName === "airpod" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("airpod")}
          >
            Air Pod
          </div>
          <div
            className={`${productName === "watch" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("watch")}
          >
            Watch
          </div>
          <div className={styles["title-nav"]}>OTHER</div>
          <div
            className={`${productName === "mouse" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("mouse")}
          >
            Mouse
          </div>
          <div
            className={`${productName === "keyboard" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("keyboard")}
          >
            Keyboard
          </div>
          <div
            className={`${productName === "other" ? styles.isActive : ""} ${
              styles["nav-option"]
            }`}
            onClick={() => filterProductHandler("other")}
          >
            Other
          </div>
        </div>
      </nav>
      <div className={styles.content}>
        <div className={styles["actions-bar"]}>
          <input id="search" type="text" placeholder="Enter Search Here" />
          <select className={styles["sort-select"]}>
            <option>Default Sorting </option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div>
        <div className={styles["product-container"]}>{dataRender}</div>
      </div>
    </div>
  );
};

export default ShopPageContent;
