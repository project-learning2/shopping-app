import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import styles from "./RelatedProducts.module.css";

const RelatedProducts = (props) => {
  const [dataProduct, setDataProduct] = useState(null);
  const params = useParams();

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_URL + "/get-products")
      .then((response) => {
        setDataProduct(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const dataRender = dataProduct ? (
    (() => {
      let result = null;
      let relatedArr = [];
      dataProduct.forEach((el) => {
        if (el.category === props.category && params.idProduct !== el._id) {
          relatedArr.push(el);
        }
      });
      result = relatedArr.map((el) => {
        return (
          <div key={el._id} className={styles["related-product"]}>
            <div className={styles.img}>
              <img src={el.img1} alt="" />
            </div>
            <div className={styles.text}>
              <h4>{el.name}</h4>
              <p>
                {el.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND
              </p>
            </div>
          </div>
        );
      });

      return result;
    })()
  ) : (
    <p>Loadinggg........</p>
  );
  return <div className={styles["related-products"]}>{dataRender}</div>;
};

export default RelatedProducts;
