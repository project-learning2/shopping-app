import styles from "./BannerShopPage.module.css";

const BannerShopPage = () => {
  return (
    <div className={styles["banner-shop"]}>
      <h2>SHOP</h2>
      <p>SHOP</p>
    </div>
  );
};

export default BannerShopPage;
