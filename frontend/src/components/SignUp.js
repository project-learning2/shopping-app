import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

import styles from "./SignUp.module.css";
import Button from "./UI/Button";

const SignUp = () => {
  const navigate = useNavigate();
  const [enteredFullName, setEnteredFullName] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [enteredPhone, setEnteredPhone] = useState("");
  const [message, setMessage] = useState(null);

  const changeFullNameHandler = (events) => {
    setEnteredFullName(events.target.value);
  };
  const changeEmailHandler = (events) => {
    setEnteredEmail(events.target.value);
  };
  const changePasswordHandler = (events) => {
    setEnteredPassword(events.target.value);
  };
  const changeTelHandler = (events) => {
    setEnteredPhone(events.target.value);
  };
  //   let alert = null;
  const formSubmitHandler = (events) => {
    events.preventDefault();
    if (
      enteredFullName.trim().length === 0 &&
      enteredEmail.trim().length === 0 &&
      enteredFullName.trim().length === 0 &&
      enteredPhone.trim().length === 0
    ) {
      setMessage(<p>Please fill Input</p>);
    } else {
      if (enteredPassword.trim().length <= 8) {
        setMessage(<p>Password need more than 8 character</p>);
      } else {
        const dataUserCreate = {
          fullName: enteredFullName,
          email: enteredEmail,
          password: enteredPassword,
          phone: enteredPhone,
        };
        axios
          .post(process.env.REACT_APP_API_URL + "/create-user", dataUserCreate)
          .then((response) => {
            alert(response.data.message);
            navigate("/login/login");
          })
          .catch((err) => {
            setMessage(err.response.data.message);
          });
      }
    }
    return;
  };
  return (
    <form onSubmit={formSubmitHandler} className={styles["sign-up"]}>
      <h3>Sign Up</h3>
      <div className={styles["message-alert"]}>{message}</div>
      <div className={styles["input-container"]}>
        <input
          placeholder="Full Name"
          type="text"
          id="full-name"
          onChange={changeFullNameHandler}
          value={enteredFullName}
        />
        <input
          placeholder="Email"
          type="email"
          id="e-mail"
          onChange={changeEmailHandler}
          value={enteredEmail}
        />
        <input
          placeholder="Password"
          type="password"
          onChange={changePasswordHandler}
          value={enteredPassword}
        />
        <input
          placeholder="Phone"
          type="tel"
          onChange={changeTelHandler}
          value={enteredPhone}
        />
      </div>
      <div className={styles.actions}>
        <Button type="submit">SIGN UP</Button>
        <p>
          Login? <Link to="/login/login">Click</Link>
        </p>
      </div>
    </form>
  );
};

export default SignUp;
