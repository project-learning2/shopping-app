import styles from "./OtherInfor.module.css";
import Button from "./UI/Button";

const OtherInfor = () => {
  return (
    <div className={styles["other-infor"]}>
      <div className={styles.informations}>
        <div className={styles.information}>
          <h3>FREE SHIPPING</h3>
          <p>Free shipping worlwide</p>
        </div>
        <div className={styles.information}>
          <h3>24X7 SERVICE</h3>
          <p>Free shipping worlwide</p>
        </div>
        <div className={styles.information}>
          <h3>FESTIVAL OFFER</h3>
          <p>Free shipping worlwide</p>
        </div>
      </div>
      <div className={styles["subscribe-form"]}>
        <div className={styles["title-subscribe-form"]}>
          <h3>LET'S BE FRIENDS!</h3>
          <p>Nisi nisi tempor consequat laboris nisi.</p>
        </div>
        <form className={styles["form-input"]}>
          <input type="email" placeholder="Enter your email address" />
          <Button>Subscribe</Button>
        </form>
      </div>
    </div>
  );
};

export default OtherInfor;
