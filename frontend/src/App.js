import { createBrowserRouter, RouterProvider } from "react-router-dom";

import HomePage from "./pages/HomePage";
import RootLayout from "./components/layout/RootLayout";
import ErrorPage from "./pages/ErrorPage";
import ShopPage from "./pages/ShopPage";
import ProductDetailPage from "./pages/ProductDetailPage";
import CartPage from "./pages/CartPage";
import CheckoutPage from "./pages/CheckoutPage";
import LoginPage from "./pages/LoginPage";
import Register from "./pages/RegisterPage";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import Test from "./pages/NewComponent";
import History from "./pages/History";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: "/", element: <HomePage />, index: true },
      { path: "/shop", element: <ShopPage /> },
      { path: "/detail/:idProduct", element: <ProductDetailPage /> },
      { path: "/cart", element: <CartPage /> },
      { path: "/checkout", element: <CheckoutPage /> },
      { path: "/test", element: <Test /> },
      { path: "/history/:userId", element: <History /> },
      {
        path: "/login",
        element: <LoginPage />,
        children: [
          {
            path: "/login/login",
            element: <Login />,
          },
          {
            path: "/login/signup",
            element: <SignUp />,
          },
        ],
      },
      { path: "/register", element: <Register /> },
    ],
  },
]);

function App() {
  console.log(process.env.REACT_APP_API_URL);
  return <RouterProvider router={router} />;
}

export default App;
