import BannerShopPage from "../components/BannerShopPage";
import ShopPageContent from "../components/ShopPageContent";

const ShopPage = () => {
  return (
    <>
      <BannerShopPage />
      <ShopPageContent />
    </>
  );
};

export default ShopPage;
