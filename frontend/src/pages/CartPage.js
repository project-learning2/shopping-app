import BannerCartPage from "../components/BannerCartPage";
import ShoppingCart from "../components/ShoppingCart";

const CartPage = () => {
  return (
    <div className="cart-page">
      <BannerCartPage />
      <ShoppingCart />
    </div>
  );
};

export default CartPage;
