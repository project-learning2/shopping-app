import BannerCheckout from "../components/BannerCheckout";
import BillDetail from "../components/BillDetail";

const CheckoutPage = () => {
  return (
    <div>
      <BannerCheckout />
      <BillDetail />
    </div>
  );
};

export default CheckoutPage;
