import { useNavigate, useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretLeft, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";

import styles from "./ProductDetailPage.module.css";
import Button from "../components/UI/Button";
import RelatedProducts from "../components/RelatedProducts";
import { counterQuantityActions } from "../store/couterQuantity";
import { cartActions } from "../store/cart";

const ProductDetailPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [dataProduct, setDataProduct] = useState(null);
  const params = useParams().idProduct;
  const counterQuantity = useSelector((state) => state.couter.counterQuantity);
  const itemCart = useSelector((state) => state.cart.cart);

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_API_URL}/get-product-by-id/${params}`)
      .then((response) => {
        setDataProduct(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const incrementHandler = () => {
    dispatch(counterQuantityActions.increment());
  };
  const decrementHandler = () => {
    if (counterQuantity > 0) {
      dispatch(counterQuantityActions.decrement());
    }
  };
  const addCartHandler = () => {
    if (counterQuantity > 0) {
      const productAdd = {
        id: params,
        quantity: counterQuantity,
      };
      dispatch(cartActions.add_cart(productAdd));
      const itemCartArr = itemCart;
      window.localStorage.setItem("itemCart", JSON.stringify(itemCartArr));
      alert("Add to cart success");
      navigate("/cart");
    } else {
      alert("Pls choose quantity");
    }
  };

  const dataRender = dataProduct ? (
    <>
      <div key={dataProduct._id} className={styles["product-detail"]}>
        <div className={styles["product-img"]}>
          <div className={styles["img-options"]}>
            <div key="img-1" className={styles["img-el"]}>
              <img src={dataProduct.img1} alt="img1" />
            </div>
            <div key="img-2" className={styles["img-el"]}>
              <img src={dataProduct.img2} alt="img2" />
            </div>
            <div key="img-3" className={styles["img-el"]}>
              <img src={dataProduct.img3} alt="img3" />
            </div>
            <div key="img-4" className={styles["img-el"]}>
              <img src={dataProduct.img4} alt="img4" />
            </div>
          </div>
          <div className={`${styles["img-detail"]} ${styles["img-el"]}`}>
            <img src={dataProduct.img1} alt="" />
          </div>
        </div>
        <div className={styles["product-overview"]}>
          <h2>{dataProduct.name}</h2>
          <h6>
            {dataProduct.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
            VND
          </h6>
          <p className={styles["short-desc"]}>{dataProduct.short_desc}</p>
          <div className={styles["category"]}>
            <span>CATEGORY: </span>
            {dataProduct.category}
          </div>
          <div className={styles["cart-action"]}>
            <div className={styles.quantity}>
              <span>QUANTITY</span>
              <div className={styles["choose-quantity"]}>
                <span onClick={decrementHandler} className={styles["add-btn"]}>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </span>
                <span> {counterQuantity} </span>
                <span
                  onClick={incrementHandler}
                  className={styles["subtract-btn"]}
                >
                  <FontAwesomeIcon icon={faCaretRight} />
                </span>
              </div>
            </div>
            <Button onClick={addCartHandler}>Add to Cart</Button>
          </div>
        </div>
      </div>
      <div className={styles["desc-detail"]}>
        <Button>DESCRIPTION</Button>
        <div className={styles["text-desc"]}>
          <h2>PRODUCT DESCRIPTION</h2>
          <div className={styles.text}>
            {dataProduct.long_desc.split("\n\n•").length > 1 && (
              <>
                <h3>{dataProduct.long_desc.split("\n\n•")[0]}</h3>
                <div>
                  {dataProduct.long_desc
                    .split("\n\n•")
                    // .split("\n•")
                    .splice(1)
                    .map((el, index) => (
                      <p key={index}>- {el}</p>
                    ))}
                </div>
              </>
            )}
            {dataProduct.long_desc.split("\n•\t").length > 1 && (
              <>
                <h3>{dataProduct.long_desc.split("\n•\t")[0]}</h3>
                <div>
                  {dataProduct.long_desc
                    .split("\n•\t")
                    // .split("\n•")
                    .splice(1)
                    .map((el, index) => (
                      <p key={index}>- {el}</p>
                    ))}
                </div>
              </>
            )}
            {dataProduct.long_desc.split("\n-").length > 1 && (
              <>
                <h3>{dataProduct.long_desc.split("\n-")[0]}</h3>
                <div>
                  {dataProduct.long_desc
                    .split("\n-")
                    // .split("\n•")
                    .splice(1)
                    .map((el, index) => (
                      <p key={index}>- {el}</p>
                    ))}
                </div>
              </>
            )}
            {dataProduct.long_desc.split("\n•        ").length > 1 && (
              <>
                <h3>{dataProduct.long_desc.split("\n•        ")[0]}</h3>
                <div>
                  {dataProduct.long_desc
                    .split("\n•        ")
                    .splice(1)
                    .map((el, index) => (
                      <p key={index}>- {el}</p>
                    ))}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
      <div className={styles["related-products"]}>
        <h2>RELATED PRODUCTS</h2>
        <RelatedProducts category={dataProduct.category} />
      </div>
    </>
  ) : (
    <p>Loadinggg........</p>
  );
  // console.log(dataRender);

  return <div className={styles["product-detail-page"]}>{dataRender}</div>;
};

export default ProductDetailPage;
