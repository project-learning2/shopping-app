import { Outlet } from "react-router-dom";

const LoginPage = () => {
  return (
    <main className="login-page">
      <Outlet />
    </main>
  );
};

export default LoginPage;
