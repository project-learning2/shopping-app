import Banner from "../components/Banner";
import Catagory from "../components/Category";
// import LiveChat from "../components/LiveChat";
import OtherInfor from "../components/OtherInfor";
import ProductList from "../components/ProductList";

const HomePage = () => {
  return (
    <>
      {/* <LiveChat /> */}
      <Banner />
      <Catagory />
      <ProductList />
      <OtherInfor />
    </>
  );
};

export default HomePage;
