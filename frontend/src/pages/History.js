import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import styles from "./History.module.css";

const History = () => {
  const params = useParams().userId;
  const [detailData, setDetailData] = useState(null);
  const [dataOrder, setDataOrder] = useState(null);
  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_API_URL}/get-history/${params}`)
      .then((response) => {
        setDataOrder(response.data);
        console.log(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const viewDetailHandle = (order) => {
    setDetailData(order);
  };

  const detailOrder = detailData && (
    <div className={styles["detail-order"]}>
      <button
        onClick={() => {
          setDetailData(null);
        }}
      >
        Close Detail
      </button>
      <div className={styles["overview"]}>
        {" "}
        <h3>IMFORMATION ORDER</h3>
        <p>ID User: {detailData.user}</p>
        <p>Full Name: {detailData.info.fullName}</p>
        <p>Phone: {detailData.info.phoneNumber}</p>
        <p>Address: {detailData.info.address}</p>
        <p>
          Total:{" "}
          {detailData.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
          VND
        </p>
      </div>
      <table className={styles["detail"]}>
        <thead>
          <tr>
            <th>ID PRODUCT</th>
            <th>IMAGE</th>
            <th>NAME</th>
            <th>PRICE</th>
            <th>COUNT</th>
          </tr>
        </thead>
        <tbody>
          {detailData.products.map((el) => {
            return (
              <tr key={el._id._id}>
                <td>{el._id._id}</td>
                <td>
                  <img src={el._id.img1} alt="" />
                </td>
                <td>{el._id.name}</td>
                <td>
                  {el._id.price
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
                  VND
                </td>
                <td>{el.quantity}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
  const orderRender =
    dataOrder &&
    dataOrder.map((el) => {
      return (
        <tr key={el._id}>
          <td>{el._id}</td>
          <td>{el.user}</td>
          <td>{el.info.fullName}</td>
          <td>{el.info.phoneNumber}</td>
          <td>{el.info.address}</td>
          <td>
            {el.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND
          </td>
          <td>Waiting for processing</td>
          <td>Waiting for pay</td>
          <td>
            <button onClick={() => viewDetailHandle(el)}>
              View <FontAwesomeIcon icon={faArrowRight} />
            </button>
          </td>
        </tr>
      );
    });

  return (
    <div className={styles["history"]}>
      <div className={styles.banner}>
        <h3>HISTORY</h3>
        <div>HISTORY</div>
      </div>
      {detailData ? (
        detailOrder
      ) : (
        <table className={styles.table}>
          <thead className={styles.thead}>
            <tr>
              <th>ID ORDER</th>
              <th>ID USER</th>
              <th>NAME</th>
              <th>PHONE</th>
              <th>ADDRESS</th>
              <th>TOTAL</th>
              <th>DELIVERY</th>
              <th>STATUS</th>
              <th>DETAIL</th>
            </tr>
          </thead>
          <tbody>{orderRender}</tbody>
        </table>
      )}
    </div>
  );
};

export default History;
