import { useEffect, useState } from "react";

const Test = () => {
  const [data, setData] = useState("");
  const [data2, setData2] = useState("");
  const changeDataHandle = (events) => {
    setData(events.target.value);
  };
  const changeDataHandle2 = (events) => {
    setData2(events.target.value);
  };
  const submitHandle = (events) => {
    events.preventDefault();
    const result = Number(data) + Number(data2);
    console.log(result);
  };
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then((response) => console.log(response.json()))
      .catch((err) => console.log(err));
  }, []);
  return (
    <form onSubmit={submitHandle}>
      <input type="number" value={data} onChange={changeDataHandle} />
      <input type="number" value={data2} onChange={changeDataHandle2} />
      <button type="submit">SUbmit</button>
    </form>
  );
};

export default Test;
