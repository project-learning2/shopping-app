import { createSlice } from "@reduxjs/toolkit";

const initialCartState = {
  cart:
    JSON.parse(window.localStorage.getItem("itemCart")) === null
      ? []
      : JSON.parse(window.localStorage.getItem("itemCart")),
};

const cartSlice = createSlice({
  name: "cart",
  initialState: initialCartState,
  reducers: {
    add_cart(state, action) {
      const itemInCart = state.cart.find(
        (item) => item.id === action.payload.id
      );
      if (itemInCart) {
        itemInCart.quantity += action.payload.quantity;
      } else {
        state.cart.push(action.payload);
      }
    },
    increment(state, action) {
      const itemInCart = state.cart.find(
        (item) => item.product === action.payload.product
      );
      itemInCart.quantity++;
    },
    decrement(state, action) {
      const itemInCart = state.cart.find(
        (item) => item.product === action.payload.product
      );
      itemInCart.quantity--;
    },
    delete_cart(state, action) {
      const removeItem = state.cart.filter(
        (item) => item.id !== action.payload.id
      );
      state.cart = removeItem;
    },
  },
});

export const cartActions = cartSlice.actions;

export default cartSlice.reducer;
