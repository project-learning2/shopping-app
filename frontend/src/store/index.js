import { configureStore } from "@reduxjs/toolkit";

import authReducer from "./auth";
import counterReducer from "./couterQuantity";
import cartReducer from "./cart";

const store = configureStore({
  reducer: { auth: authReducer, couter: counterReducer, cart: cartReducer },
});

export default store;
