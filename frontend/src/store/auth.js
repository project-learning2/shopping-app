import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
  isAuthenticated: false,
  userLogin: null,
};

const authSlice = createSlice({
  name: "authentication",
  initialState: initialAuthState,
  reducers: {
    login(state, action) {
      state.isAuthenticated = true;
      state.userLogin = action.payload;
    },
    logout(state) {
      state.isAuthenticated = false;
      state.userLogin = null;
      // window.localStorage.set("userLogin", null);
    },
  },
});

export const authActions = authSlice.actions;

export default authSlice.reducer;
