import { createSlice } from "@reduxjs/toolkit";

const initialCouterQuantityState = {
  counterQuantity: 0,
};

const counterQuantitySlice = createSlice({
  name: "counterQuantity",
  initialState: initialCouterQuantityState,
  reducers: {
    increment(state) {
      state.counterQuantity++;
    },
    decrement(state) {
      state.counterQuantity--;
    },
  },
});

export const counterQuantityActions = counterQuantitySlice.actions;

export default counterQuantitySlice.reducer;
